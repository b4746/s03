CREATE DATABASE blog_db

USE blog_db;

CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL,
    pssword VARCHAR(300) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE posts (
	id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(500) NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_created DATETIME NOT NULL,
    user_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_user_id
    	FOREIGN KEY (user_id) REFERENCES users(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);

CREATE TABLE posts_comment (
	id INT NOT NULL AUTO_INCREMENT,
    content VARCHAR(5000) NOT NULL,
    datetime_created DATETIME NOT NULL,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_comment_user_id
    	FOREIGN KEY (user_id) REFERENCES users(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT,
    CONSTRAINT fk_posts_comment_post_id
    	FOREIGN KEY (post_id) REFERENCES posts(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);

CREATE TABLE posts_likes (
	id INT NOT NULL AUTO_INCREMENT,
    datetime_created DATETIME NOT NULL,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_likes_user_id
    	FOREIGN KEY (user_id) REFERENCES users(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT,
    CONSTRAINT fk_posts_likes_post_id
    	FOREIGN KEY (post_id) REFERENCES posts(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
)


-- INSERT user
INSERT INTO users(pssword, email, datetime_created) VALUES ("passwordA","johnsmith@gmail.com", "2021-01-01 01:00:00");

INSERT INTO users(pssword, email, datetime_created) VALUES ("passwordB","juandelacruz@gmail.com", "2021-01-01 02:00:00");

INSERT INTO users(pssword, email, datetime_created) VALUES ("passwordC","janesmith@gmail.com", "2021-01-01 03:00:00");

INSERT INTO users(pssword, email, datetime_created) VALUES ("passwordD","mariadelacruz@gmail.com", "2021-01-01 04:00:00");

INSERT INTO users(pssword, email, datetime_created) VALUES ("passwordE","johndoe@gmail.com", "2021-01-01 05:00:00");



-- INSERT post
INSERT INTO posts(user_id, title, datetime_created, content) VALUES ("1","First Code", "2021-01-02 01:00:00", "Hello World!");

INSERT INTO posts(user_id, title, datetime_created, content) VALUES ("1","Second Code", "2021-01-02 02:00:00", "Hello Earth!");

INSERT INTO posts(user_id, title, datetime_created, content) VALUES ("2","Third Code", "2021-01-02 03:00:00", "Welcome to Mars!");

INSERT INTO posts(user_id, title, datetime_created, content) VALUES ("4","Fourth Code", "2021-01-02 04:00:00", "Bye bye solar system!");


-- Get all the title with a user ID of 1.
SELECT title FROM posts WHERE user_id = 1;

-- Get all the user's email and datetime of creation.
SELECT email, datetime_created FROM users;

-- Update a post.....
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content="Hello Earth";

-- Delete user with an email of "johndoe@gmail.com"
DELETE FROM users WHERE email ="johndoe@gmail.com";

